#ifndef CARDS_GAME_H
#define CARDS_GAME_H

#include "Player.h"
#include <vector>

namespace cards {
	class Game {
	public:
		virtual int play() = 0;

	protected:
		int nPlayers;
		std::vector<Player> players;
		std::vector<Player>::iterator dealerIt;

		virtual void createPlayers();
		virtual void dealHands();
		virtual std::vector<Card> getSortReference() const;
		virtual std::vector<Suit> getSuitOrder() const;
		virtual std::vector<Face> getFaceOrder() const;

		void printStack(const Stack& stack) const;
		void printHand(const Player& player) const;

		void printDivider() const;

		Card selectCard(const Player& player) const;
		virtual std::vector<bool> getPlayableCards(const Player& player) const;

		void incrementPlayerIterator(std::vector<Player>::iterator& iterator);
	};
}

#endif
