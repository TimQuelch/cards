#include "Game.h"
#include "Deck.h"
#include <iomanip>
#include <iostream>
#include <sstream>

namespace cards {
	void Game::createPlayers() {
		players.reserve(nPlayers);
		for (int i = 0; i < nPlayers; i++) {
			players.emplace_back();
		}
	}

	void Game::dealHands() {
		Deck deck(DeckType::oneJoker);
		const int handSize = DECK_LENGTH_ONEJOKER / nPlayers;

		for (Player& player : players) {
			deck.dealCards(handSize, player.getHand());
		}

		std::vector<Player>::iterator extraCard = dealerIt;
		incrementPlayerIterator(extraCard);

		while (deck.getSize() > 0) {
			deck.dealCards(1, extraCard->getHand());
			incrementPlayerIterator(extraCard);
		}
	}

	std::vector<Card> Game::getSortReference() const {
		std::vector<Card> order;
		order.reserve(DECK_LENGTH_ONEJOKER);
		for (Suit suit : getSuitOrder()) {
			for (Face face : getFaceOrder()) {
				order.push_back(Card(suit, face));
			}
		}
		order.push_back(Card(Suit::joker, Face::joker));
		return order;
	}

	std::vector<Suit> Game::getSuitOrder() const {
		std::vector<Suit> order = {Suit::spade, Suit::club, Suit::diamond, Suit::heart};
		return order;
	}

	std::vector<Face> Game::getFaceOrder() const {
		std::vector<Face> order = {Face::two,
		                           Face::three,
		                           Face::four,
		                           Face::five,
		                           Face::six,
		                           Face::seven,
		                           Face::eight,
		                           Face::nine,
		                           Face::ten,
		                           Face::jack,
		                           Face::queen,
		                           Face::king,
		                           Face::ace};
		return order;
	}

	void Game::printStack(const Stack& stack) const {
		const std::vector<Card> cards = stack.getCards();
		for (Card card : cards) {
			std::cout << card.getString() << " ";
		}
		std::cout << std::endl;
	}

	void Game::printHand(const Player& player) const {
		printStack(player.getHand());
	}

	void Game::printDivider() const {
		std::cout << "--------------------------------------------------" << std::endl;
	}

	Card Game::selectCard(const Player& player) const {
		const std::vector<Card> cards = player.getHand().getCards();
		const std::vector<bool> playableCards = getPlayableCards(player);

		printHand(player);
		unsigned id = 0;
		for (bool playable : playableCards) {
			if (playable) {
				std::cout << std::setw(3) << ++id;
			} else {
				std::cout << "   ";
			}
			std::cout << " ";
		}
		std::cout << std::endl;

		bool validInput = false;
		std::string rawInput;
		unsigned input;
		while (!validInput) {
			std::cout << "Select a card to play: ";
			std::getline(std::cin, rawInput);
			std::stringstream sstream(rawInput);
			if (sstream >> input && input > 0 && input <= id) {
				validInput = true;
			} else {
				std::cout << "Invalid input." << std::endl;
			}
		}

		unsigned selection = 0;
		unsigned index = 0;
		std::vector<bool>::const_iterator playableIt = playableCards.cbegin();
		while (selection != input) {
			if (*playableIt) {
				selection++;
			}
			index++;
		}
		return cards[index - 1];
	}

	std::vector<bool> Game::getPlayableCards(const Player& player) const {
		return std::vector<bool>(player.getHandSize(), true); // Default is all cards are playable
	}

	void Game::incrementPlayerIterator(std::vector<Player>::iterator& iterator) {
		if (++iterator == players.end()) {
			iterator = players.begin();
		}
	}
}
