#ifndef CARDS_WARLORDSGAME_H
#define CARDS_WARLORDSGAME_H

#include "Game.h"

namespace cards {
	const int PLAYER_N_WARLORDS_MIN = 3;
	const int PLAYER_N_WARLORDS_MAX = 8;
	const int PLAYER_N_WARLORDS_DEFAULT = 4;

	class WarlordsGame : public Game {
	public:
		WarlordsGame();
		WarlordsGame(int nPlayers);

		int play();

	private:
		void startGame();

		std::vector<Card> getSortReference() const;
		std::vector<Face> getFaceOrder() const;
	};
}

#endif
