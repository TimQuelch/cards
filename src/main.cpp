#include "FiveHundredGame.h"
#include "HeartsGame.h"
#include "WarlordsGame.h"

int main(int argc, char* argv[]) {
	cards::HeartsGame game;
	// cards::FiveHundredGame game;
	// cards::WarlordsGame game;
	return game.play();
}
