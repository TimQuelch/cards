#include "FiveHundredGame.h"
#include "Deck.h"
#include <algorithm>
#include <iostream>
#include <vector>

namespace cards {
	FiveHundredGame::FiveHundredGame() {
		nPlayers = 4;
		trumpSuit = Suit::diamond;
		createPlayers();
		dealerIt = players.begin();
		callerIt = dealerIt + 1;
	}

	int FiveHundredGame::play() {
		startGame();
		return 0;
	}

	void FiveHundredGame::startGame() {
		dealHands();
		for (unsigned i = 0; i < players.size(); i++) {
			std::cout << "Player " << (i + 1) << std::endl;
			printHand(players[i]);
			players[i].getHand().sort(getSortReference());
			printHand(players[i]);
		}
		std::cout << "Kitty" << std::endl;
		std::vector<Card> cards = kitty.getCards();
		for (Card card : cards) {
			std::cout << card.getString() << " ";
		}
		std::cout << std::endl;
	}

	void FiveHundredGame::dealHands() {
		Deck fiveHundredDeck(DeckType::fiveHundred);
		const int handSize = 10;
		const int kittySize = 3;

		for (Player& player : players) {
			fiveHundredDeck.dealCards(handSize, player.getHand());
		}
		fiveHundredDeck.dealCards(kittySize, kitty);
	}

	std::vector<Card> FiveHundredGame::getSortReference() const {
		using std::find;
		std::vector<Card> ref = Game::getSortReference();
		const Card js(Suit::spade, Face::jack);
		const Card jc(Suit::club, Face::jack);
		const Card jd(Suit::diamond, Face::jack);
		const Card jh(Suit::heart, Face::jack);
		const Card joker(Suit::joker, Face::joker);
		if (trumpSuit == Suit::joker) {
			return ref;
		} else if (trumpSuit == Suit::spade) {
			const Card next = *(++find(ref.begin(), ref.end(), Card(Suit::spade, Face::ace)));
			ref.erase(find(ref.begin(), ref.end(), jc));
			ref.erase(find(ref.begin(), ref.end(), js));
			ref.erase(find(ref.begin(), ref.end(), joker));
			ref.insert(find(ref.begin(), ref.end(), next), jc);
			ref.insert(find(ref.begin(), ref.end(), next), js);
			ref.insert(find(ref.begin(), ref.end(), next), joker);
		} else if (trumpSuit == Suit::club) {
			const Card next = *(++find(ref.begin(), ref.end(), Card(Suit::club, Face::ace)));
			ref.erase(find(ref.begin(), ref.end(), js));
			ref.erase(find(ref.begin(), ref.end(), jc));
			ref.erase(find(ref.begin(), ref.end(), joker));
			ref.insert(find(ref.begin(), ref.end(), next), js);
			ref.insert(find(ref.begin(), ref.end(), next), jc);
			ref.insert(find(ref.begin(), ref.end(), next), joker);
		} else if (trumpSuit == Suit::diamond) {
			const Card next = *(++find(ref.begin(), ref.end(), Card(Suit::diamond, Face::ace)));
			ref.erase(find(ref.begin(), ref.end(), jh));
			ref.erase(find(ref.begin(), ref.end(), jd));
			ref.erase(find(ref.begin(), ref.end(), joker));
			ref.insert(find(ref.begin(), ref.end(), next), jh);
			ref.insert(find(ref.begin(), ref.end(), next), jd);
			ref.insert(find(ref.begin(), ref.end(), next), joker);
		} else { // trumpSuit == Suit::heart
			const Card next = *(++find(ref.begin(), ref.end(), Card(Suit::heart, Face::ace)));
			ref.erase(find(ref.begin(), ref.end(), jd));
			ref.erase(find(ref.begin(), ref.end(), jh));
			ref.erase(find(ref.begin(), ref.end(), joker));
			ref.insert(find(ref.begin(), ref.end(), next), jd);
			ref.insert(find(ref.begin(), ref.end(), next), jh);
			ref.insert(find(ref.begin(), ref.end(), next), joker);
		}
		return ref;
	}
}
