#ifndef CARDS_CARD_H
#define CARDS_CARD_H

#include <string>

namespace cards {
	const int FACE_N = 13;
	enum class Face {
		ace = 1,
		two,
		three,
		four,
		five,
		six,
		seven,
		eight,
		nine,
		ten,
		jack,
		queen,
		king,
		joker,
		begin = (int)ace,
		end = (int)joker
	};
	Face increment(Face& face);

	const int SUIT_N = 4;
	enum class Suit { spade, club, diamond, heart, joker, begin = (int)spade, end = (int)joker };
	Suit increment(Suit& suit);

	class Card {
	public:
		Card(Suit suit, Face face);

		bool operator==(const Card& rhs) const;
		bool operator!=(const Card& rhs) const;

		Face getFace() const;
		Suit getSuit() const;
		std::string getStringSuit() const;
		std::string getStringFace() const;
		std::string getString() const;

		void setFace(Face newFace);
		void setSuit(Suit newSuit);

	private:
		Face face;
		Suit suit;
	};
}
#endif
