#ifndef CARDS_FIVEHUNDREDGAME_H
#define CARDS_FIVEHUNDREDGAME_H

#include "Game.h"

namespace cards {
	const int PLAYER_N_FIVEHUNDRED = 4;

	class FiveHundredGame : public Game {
	public:
		FiveHundredGame();

		int play();

	private:
		void startGame();
		void dealHands();

		Stack playing;
		Stack kitty;

		Suit trumpSuit; // Joker for No Trumps/Misere

		std::vector<Player>::iterator dealerIt;
		std::vector<Player>::iterator callerIt;

		std::vector<Card> getSortReference() const;
	};
}

#endif
