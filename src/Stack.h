#ifndef CARDS_STACK_H
#define CARDS_STACK_H

#include "Card.h"
#include <random>
#include <vector>

namespace cards {
	class Stack {
	public:
		void shuffle();
		void transferCard(std::vector<Card>::iterator cardIt, Stack& stack);
		void transferCard(const Card& card, Stack& stack);
		void transferCard(Stack& stack);
		void receiveCard(const Card& card);
		void sort(std::vector<Card> reference);
		int getSize() const;
		std::vector<Card> getCards() const;

	protected:
		std::vector<Card> cards;
		static std::default_random_engine rng;
	};
}

#endif
