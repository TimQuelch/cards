#include "Deck.h"

namespace cards {
	Deck::Deck(DeckType deckType) {
		if (deckType != DeckType::fiveHundred) {
			for (Suit suit = Suit::begin; suit != Suit::joker; increment(suit)) {
				for (Face face = Face::begin; face != Face::joker; increment(face)) {
					cards.push_back(Card(suit, face));
				}
			}
		} else {
			cards.push_back(Card(Suit::heart, Face::four));
			cards.push_back(Card(Suit::diamond, Face::four));
			for (Suit suit = Suit::begin; suit != Suit::joker; increment(suit)) {
				for (Face face = Face::five; face != Face::joker; increment(face)) {
					cards.push_back(Card(suit, face));
				}
				cards.push_back(Card(suit, Face::ace));
			}
		}

		if (deckType == DeckType::oneJoker || deckType == DeckType::fiveHundred) {
			cards.push_back(Card(Suit::joker, Face::joker));
		} else if (deckType == DeckType::twoJokers) {
			cards.push_back(Card(Suit::joker, Face::joker));
			cards.push_back(Card(Suit::joker, Face::joker));
		}

		shuffle();
	}

	void Deck::dealCards(int numberOfCards, Stack& stack) {
		for (int i = 0; i < numberOfCards; i++) {
			transferCard(--cards.end(), stack);
		}
	}
}
