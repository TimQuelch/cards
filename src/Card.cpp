#include "Card.h"

namespace cards {
	Face increment(Face& face) {
		if (face != Face::end) {
			face = (Face)((int)face + 1);
		} else {
			face = Face::begin;
		}
		return face;
	}

	Suit increment(Suit& suit) {
		if (suit != Suit::end) {
			suit = (Suit)((int)suit + 1);
		} else {
			suit = Suit::begin;
		}
		return suit;
	}

	Card::Card(Suit suit, Face face) {
		this->suit = suit;
		this->face = face;
	}

	bool Card::operator==(const Card& rhs) const {
		return (face == rhs.face) && (suit == rhs.suit);
	}

	bool Card::operator!=(const Card& rhs) const {
		return !(*this == rhs);
	}

	Face Card::getFace() const {
		return face;
	}

	Suit Card::getSuit() const {
		return suit;
	}

	void Card::setFace(Face newFace) {
		face = newFace;
	}

	void Card::setSuit(Suit newSuit) {
		suit = newSuit;
	}

	std::string Card::getStringSuit() const {
		switch (suit) {
		case Suit::spade:
			return std::string("S");
		case Suit::club:
			return std::string("C");
		case Suit::diamond:
			return std::string("D");
		case Suit::heart:
			return std::string("H");
		case Suit::joker:
			return std::string("*");
		}
		return std::string("?");
	}

	std::string Card::getStringFace() const {
		switch (face) {
		case Face::ace:
			return std::string(" A");
		case Face::two:
			return std::string(" 2");
		case Face::three:
			return std::string(" 3");
		case Face::four:
			return std::string(" 4");
		case Face::five:
			return std::string(" 5");
		case Face::six:
			return std::string(" 6");
		case Face::seven:
			return std::string(" 7");
		case Face::eight:
			return std::string(" 8");
		case Face::nine:
			return std::string(" 9");
		case Face::ten:
			return std::string("10");
		case Face::jack:
			return std::string(" J");
		case Face::queen:
			return std::string(" Q");
		case Face::king:
			return std::string(" K");
		case Face::joker:
			return std::string(" J");
		}
		return std::string("??");
	}

	std::string Card::getString() const {
		return std::string(getStringFace() + getStringSuit());
	}
}
