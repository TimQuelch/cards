#ifndef CARDS_HEARTSGAME_H
#define CARDS_HEARTSGAME_H

#include "Game.h"

namespace cards {
	const int PLAYER_N_HEARTS_MIN = 3;
	const int PLAYER_N_HEARTS_MAX = 6;
	const int PLAYER_N_HEARTS_DEFAULT = 4;

	class HeartsGame : public Game {
	public:
		HeartsGame();
		HeartsGame(int nPlayers);

		int play();

	private:
		void dealHands();

		void startGame();
		void startHand();
		void startTrick();

		Player& findTrickWinner(const std::vector<std::pair<Player*, Card>>& order) const;
		void addTrickScore(const Player& winner);
		unsigned countPointsInTrick() const;

		void addGameScores();

		bool checkGameEnd() const;
		bool checkHandEnd() const;
		bool checkTrickEnd() const;

		void discardPlaying();

		void printScores(const std::vector<int>& scores);
		void printTrickScores();
		void printGameScores();

		void resetTrickScores();
		void resetGameScores();

		std::vector<int> trickScores;
		std::vector<int> gameScores;
		Stack playing;
		Stack discard;
		Stack throwCards;
	};
}

#endif
