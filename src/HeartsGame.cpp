#include "HeartsGame.h"
#include "Deck.h"
#include <algorithm>
#include <iomanip>
#include <iostream>

namespace cards {
	HeartsGame::HeartsGame() : HeartsGame(PLAYER_N_HEARTS_DEFAULT) {
	}

	HeartsGame::HeartsGame(int nPlayers) {
		if (nPlayers > PLAYER_N_HEARTS_MAX) {
			std::cerr << "Too many players for Hearts: " << nPlayers
			          << ". Using max players: " << PLAYER_N_HEARTS_MAX << std::endl;
			this->nPlayers = PLAYER_N_HEARTS_MAX;
		} else if (nPlayers < PLAYER_N_HEARTS_MIN) {
			std::cerr << "Too few players for Hearts: " << nPlayers
			          << ". Using min players: " << PLAYER_N_HEARTS_MIN << std::endl;
			this->nPlayers = PLAYER_N_HEARTS_MIN;
		} else {
			this->nPlayers = nPlayers;
		}

		createPlayers();
	}

	int HeartsGame::play() {
		startGame();
		return 0;
	}

	void HeartsGame::dealHands() {
		Deck deck(DeckType::standard);
		const int handSize = DECK_LENGTH_STANDARD / nPlayers;

		for (Player& player : players) {
			deck.dealCards(handSize, player.getHand());
		}

		int nThrowCards = DECK_LENGTH_STANDARD - (nPlayers * handSize);
		deck.dealCards(nThrowCards, throwCards);
	}

	void HeartsGame::resetTrickScores() {
		trickScores = std::vector<int>(nPlayers, 0);
	}

	void HeartsGame::resetGameScores() {
		gameScores = std::vector<int>(nPlayers, 0);
	}

	void HeartsGame::startGame() {
		dealerIt = players.begin();
		resetGameScores();
		while (!checkGameEnd()) {
			startHand();
		}
	}

	void HeartsGame::startHand() {
		resetTrickScores();
		dealHands();
		for (Player player : players) {
			player.getHand().sort(getSortReference());
		}
		while (!checkHandEnd()) {
			startTrick();
		}
		addGameScores();
		printGameScores();
	}

	void HeartsGame::startTrick() {
		std::vector<Player>::iterator playerIt = dealerIt;
		incrementPlayerIterator(playerIt); // Left of dealer starts
		std::vector<std::pair<Player*, Card>> playerCardOrder;
		playerCardOrder.reserve(nPlayers);
		while (!checkTrickEnd()) {
			std::cout << std::endl;
			std::cout << "Player " << playerIt->getId() << std::endl;
			Card playedCard = selectCard(*playerIt);
			std::cout << "Played " << playedCard.getString() << std::endl;
			playerIt->getHand().transferCard(playedCard, playing);
			playerCardOrder.push_back(std::make_pair(&(*playerIt), playedCard));
			std::cout << "Current playing stack: " << std::endl;
			printStack(playing);
			incrementPlayerIterator(playerIt);
		}
		addTrickScore(findTrickWinner(playerCardOrder));
		printTrickScores();
		discardPlaying();
	}

	Player& HeartsGame::findTrickWinner(const std::vector<std::pair<Player*, Card>>& order) const {
		const std::vector<Face> reference = getFaceOrder();
		auto b = reference.begin();
		auto e = reference.end();

		auto current = order.begin();
		Player& winningPlayer = *(current->first);
		Card winningCard = current->second;
		const Suit leadSuit = current->second.getSuit();

		auto higherFace = [b, e](Face lhs, Face rhs) -> bool {
			return std::find(b, e, lhs) < std::find(b, e, rhs);
		};

		while (++current != order.end()) {
			Card card = current->second;
			if (card.getSuit() == leadSuit && higherFace(winningCard.getFace(), card.getFace())) {
				winningCard = card;
				winningPlayer = *(current->first);
			}
		}
		return winningPlayer;
	}

	void HeartsGame::addTrickScore(const Player& winner) {
		auto currentPlayer = players.cbegin();
		auto currentScore = trickScores.begin();
		while (*currentPlayer != winner) {
			currentPlayer++;
			currentScore++;
		}
		*currentScore += countPointsInTrick();
	}

	unsigned HeartsGame::countPointsInTrick() const {
		const std::vector<Card> cards = playing.getCards();
		unsigned points = 0;
		for (Card card : cards) {
			if (card.getSuit() == Suit::heart) {
				points += 1;
			} else if (card == Card(Suit::spade, Face::queen)) {
				points += 13;
			}
		}
		return points;
	}

	// Add trick scores to corresponding game scores
	void HeartsGame::addGameScores() {
		std::transform(gameScores.begin(),
		               gameScores.end(),
		               trickScores.begin(),
		               gameScores.begin(),
		               std::plus<int>());
	}

	bool HeartsGame::checkGameEnd() const {
		// Check if anyone is over 100 points
		for (int i : gameScores) {
			if (i > 100) {
				return true;
			}
		}
		return false;
	}

	bool HeartsGame::checkHandEnd() const {
		// All hands should be same size check first hand
		return players.begin()->getHandSize() == 0;
	}

	bool HeartsGame::checkTrickEnd() const {
		// Check if everyone has played
		return playing.getSize() == nPlayers;
	}

	void HeartsGame::discardPlaying() {
		playing.transferCard(discard);
	}

	void HeartsGame::printScores(const std::vector<int>& scores) {
		for (int score : scores) {
			std::cout << std::setw(3) << score << " ";
		}
		std::cout << std::endl;
	}

	void HeartsGame::printTrickScores() {
		printDivider();
		std::cout << "Trick scores:\n";
		printScores(trickScores);
		printDivider();
	}

	void HeartsGame::printGameScores() {
		printDivider();
		std::cout << "Scores:\n";
		printScores(gameScores);
		printDivider();
	}
}
