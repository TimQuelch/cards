#ifndef CARDS_PLAYER_H
#define CARDS_PLAYER_H

#include "Stack.h"

namespace cards {
	class Player {
	public:
		Player();
		Stack& getHand();
		const Stack& getHand() const;
		unsigned getHandSize() const;
		unsigned getId() const;

	private:
		static unsigned idCounter;
		unsigned id;
		Stack hand;
	};

	bool operator==(const Player& lhs, const Player& rhs);
	bool operator!=(const Player& lhs, const Player& rhs);
}

#endif
