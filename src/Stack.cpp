#include "Stack.h"
#include <algorithm>
#include <chrono>

namespace cards {
	using std::chrono::system_clock;
	std::default_random_engine Stack::rng(system_clock::now().time_since_epoch().count());

	void Stack::shuffle() {
		std::shuffle(cards.begin(), cards.end(), rng);
	}

	void Stack::transferCard(std::vector<Card>::iterator cardIt, Stack& stack) {
		stack.receiveCard(*cardIt);
		cards.erase(cardIt);
	}

	void Stack::transferCard(const Card& card, Stack& stack) {
		std::vector<Card>::iterator cardIt = std::find(cards.begin(), cards.end(), card);
		if (cardIt != cards.end()) {
			stack.receiveCard(card);
			cards.erase(cardIt);
		}
	}

	void Stack::transferCard(Stack& stack) {
		for (Card card : cards) {
			stack.receiveCard(card);
		}
		cards.clear();
	}

	void Stack::receiveCard(const Card& card) {
		cards.push_back(card);
	}

	void Stack::sort(std::vector<Card> reference) {
		std::vector<Card>::iterator b = reference.begin();
		std::vector<Card>::iterator e = reference.end();
		auto greaterThan = [b, e](Card lhs, Card rhs) -> bool {
			return std::find(b, e, lhs) < std::find(b, e, rhs);
		};
		std::sort(cards.begin(), cards.end(), greaterThan);
	}

	int Stack::getSize() const {
		return (int)cards.size();
	}

	std::vector<Card> Stack::getCards() const {
		return cards;
	}
}
