#include "Player.h"

namespace cards {
	unsigned Player::idCounter = 0;

	Player::Player() {
		idCounter++;
		id = idCounter;
	}

	Stack& Player::getHand() {
		return hand;
	}

	const Stack& Player::getHand() const {
		return hand;
	}

	unsigned Player::getHandSize() const {
		return hand.getSize();
	}

	unsigned Player::getId() const {
		return id;
	}

	bool operator==(const Player& lhs, const Player& rhs) {
		return lhs.getId() == rhs.getId();
	}

	bool operator!=(const Player& lhs, const Player& rhs) {
		return !(lhs == rhs);
	}

}
