#include "WarlordsGame.h"
#include "Deck.h"
#include <iostream>

namespace cards {
	WarlordsGame::WarlordsGame() : WarlordsGame(PLAYER_N_WARLORDS_DEFAULT) {
	}

	WarlordsGame::WarlordsGame(int nPlayers) {
		if (nPlayers > PLAYER_N_WARLORDS_MAX) {
			std::cerr << "Too many players for Warlords: " << nPlayers
			          << ". Using max players: " << PLAYER_N_WARLORDS_MAX << std::endl;
			this->nPlayers = PLAYER_N_WARLORDS_MAX;
		} else if (nPlayers < PLAYER_N_WARLORDS_MIN) {
			std::cerr << "Too few players for Warlords: " << nPlayers
			          << ". Using min players: " << PLAYER_N_WARLORDS_MIN << std::endl;
			this->nPlayers = PLAYER_N_WARLORDS_MIN;
		} else {
			this->nPlayers = nPlayers;
		}

		createPlayers();
		dealerIt = players.begin();
	}

	int WarlordsGame::play() {
		startGame();
		return 0;
	}

	void WarlordsGame::startGame() {
		dealHands();
		std::cout << nPlayers << std::endl;
		for (unsigned i = 0; i < players.size(); i++) {
			std::cout << "Player " << (i + 1) << std::endl;
			printHand(players[i]);
			players[i].getHand().sort(getSortReference());
			printHand(players[i]);
		}
	}

	std::vector<Card> WarlordsGame::getSortReference() const {
		std::vector<Card> order;
		order.reserve(DECK_LENGTH_ONEJOKER);
		for (Face face : getFaceOrder()) {
			for (Suit suit : getSuitOrder()) {
				order.push_back(Card(suit, face));
			}
		}
		order.push_back(Card(Suit::joker, Face::joker));
		return order;
	}

	std::vector<Face> WarlordsGame::getFaceOrder() const {
		std::vector<Face> order = {Face::three,
		                           Face::four,
		                           Face::five,
		                           Face::six,
		                           Face::seven,
		                           Face::eight,
		                           Face::nine,
		                           Face::ten,
		                           Face::jack,
		                           Face::queen,
		                           Face::king,
		                           Face::ace,
		                           Face::two};
		return order;
	}
}
