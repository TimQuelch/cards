#ifndef CARDS_DECK_H
#define CARDS_DECK_H

#include "Stack.h"

namespace cards {
	enum class DeckType { standard, oneJoker, twoJokers, fiveHundred };

	const int DECK_LENGTH_STANDARD = 52;
	const int DECK_LENGTH_ONEJOKER = 53;
	const int DECK_LENGTH_TWOJOKER = 54;
	const int DECK_LENGTH_FIVEHUNDRED = 43;

	class Deck : public Stack {
	public:
		Deck(DeckType deckType);
		void dealCards(int numberOfCards, Stack& stack);
	};
}

#endif
